#ifndef MyClass_h
#define MyClass_h

#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>

#include "secrets.h"


class Communicator {
public:
  Communicator(WiFiClient&, void (*mqttCallback)(char*, byte*, unsigned int));

  void (*mqttCallback)(char*, byte*, unsigned int);
  bool ConnectMQTT(const String&, const String&, const String&, const String&);
  void mqtt_discovery(const String, StaticJsonDocument<1536>&);
  void publish_data(String, String);

  void loop();
private:
  PubSubClient _mqtt_client;
  WiFiClient _net;
};
#endif
