#ifndef TOOLS_h
#define TOOLS_h
const char version[] = "1.0.0"
#endif

String slugify(String input) {
  input.toLowerCase();
  input.replace(" ", "_");
  return input;
}
