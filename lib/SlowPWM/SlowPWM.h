#ifndef SLOWPWM_h
#define SLOWPWM_h

#include <Arduino.h>

class slowPWM {
  private:
    byte outputPin;
    unsigned long period;
    unsigned long lastSwitchTime;
    byte outputState;

  public:
    void begin(byte pin, unsigned long per) {
      outputPin = pin;
      period = per;
      lastSwitchTime = 0;
      outputState = LOW;
      pinMode(pin, OUTPUT);
      Serial.println("Setup PWM");
    }

    void compute(byte duty) {
      unsigned long onTime = (duty * period) / 100;
      unsigned long offTime = period - onTime;
      unsigned long currentTime = millis();

      if (duty == 0) {
        outputState = LOW;
      } else if (outputState == HIGH && (currentTime - lastSwitchTime >= onTime)) {
        lastSwitchTime = currentTime;
        outputState = LOW;

      } else if (outputState == LOW && (currentTime - lastSwitchTime >= offTime)) {
        lastSwitchTime = currentTime;
        outputState = HIGH;
      }
      digitalWrite(outputPin, outputState);
    }
};
#endif
