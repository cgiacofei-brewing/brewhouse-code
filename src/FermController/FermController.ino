//#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <IPAddress.h>

// My Libraries
#include <secrets.h>
#include <global.h>
#include "config.h"
#include <Communicator.h>
#include <Tools.h>

String chiller_state = "idle";
int tank_setpoint = 28;

WiFiClient net;

void mqttCallback(char *topic, byte *payload, unsigned int length) {
  Serial.print("incoming: ");
  Serial.println(topic);
  for (unsigned int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
  }
  Serial.println("");
}

Communicator hass_comm = Communicator(net, &mqttCallback);

/* Setup MQTT discovery for chiller tank control.
 *
 * Using climate device.
 */
void climateDevice(String name, boolean multimode=false) {
  auto chipid = String(ESP.getChipId(), HEX);

  String name_slug = slugify(name);

  String config_topic = "homeassistant/climate/" + name_slug + "_" + chipid + "/config";
  String topic_root = "brewhouse/" + name_slug + "/";

  StaticJsonDocument<1536> entity;
  entity["uniq_id"] = chipid + "_" + name_slug;
  entity["name"] =  name;
  entity["temp_unit"] =  "F";

  // Mode setup
  entity["mode_cmd_t"] = topic_root + "mode/set";
  entity["mode_cmd_tpl"] = "{{ value }}";
  entity["mode_stat_t"] = topic_root + "mode/state";
  entity["mode_stat_tpl"] = "{{ value_json }}";
  JsonArray modes = entity.createNestedArray("modes");
  modes.add("off");
  modes.add("cool");

  entity["temp_cmd_t"] = topic_root + "temp/set";
  entity["temp_cmd_tpl"] = "{{ value }}";
  entity["temp_stat_t"] = topic_root + "temp/state";
  entity["temp_stat_tpl"] = "{{ value_json }}";
  entity["curr_temp_t"] = topic_root + "temp/current";
  entity["curr_temp_tpl"] = "{{ value }}";

  if (multimode == true) {
    entity["temp_hi_cmd_t"] = topic_root + "temp_hi/set";
    entity["temp_hi_cmd_tpl"] = "{{ value }}";
    entity["temp_hi_stat_t"] = topic_root + "temp_hi/state";
    entity["temp_hi_stat_tpl"] = "{{ value_json }}";

    entity["temp_lo_cmd_t"] = topic_root + "temp_lo/set";
    entity["temp_lo_cmd_tpl"] = "{{ value }}";
    entity["temp_lo_stat_t"] = topic_root + "temp_lo/state";
    entity["temp_lo_stat_tpl"] = "{{ value_json }}";
    modes.add("heat");
    modes.add("auto");
  }
  JsonObject dev = entity.createNestedObject("dev");
  dev["name"] = DEVICE_NAME;
  dev["mdl"] = DEVICE_MDL;
  dev["sw"] = String(version);
  dev["mf"] = DEVICE_MF;
  JsonArray ids = dev.createNestedArray("ids");
  ids.add(chipid);
  //dev["ids"] = "[\"" + chipid +"\"]";

  hass_comm.mqtt_discovery(config_topic, entity);
}

void setup() {
  const char* ssid = WIFI_SSID;
  const char* password = WIFI_PASSWORD;
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("WiFi Failed!");
    return;
  }
  Serial.println();
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  climateDevice("Coolant Tank");

  String f_name = "Fermenter ";
  for (int i=0;i<FERMENTER_COUNT;i++) {
    int f_num = i+1;

    Serial.println(f_name + f_num);
    climateDevice(f_name + f_num, true);
  }
}

void loop() {
  delay(5000);
  hass_comm.publish_data("brewhouse/coolant_tank/temp/current", String(tank_setpoint));
  hass_comm.publish_data("brewhouse/coolant_tank/mode/state", chiller_state);
  hass_comm.loop();
}
