/* This file is all the stuff I want to be
able to change without having to update the
source repository.
*/

#ifndef CONFIG_H
#define CONFIG_H

const char version[] = "0.0.1"

#define DEVICE_NAME "Glycol Chiller"
#define DEVICE_MDL "Chillenator v0.1"
#define DEVICE_MF "Damn Yankee Brewing"
#define FERMENTER_COUNT 2

// Pin Definitions
#define I_CLK    2
#define I_DT     3
#define c    4
#define O_PWM    5

#define I_CS1   47
#define I_CS2   48

// MQTT Topic Definitions
#define TOPIC_ROOT "brewery/"
#define BOIL_SETPOINT_TOPIC "setpoint/boil"
#define MASH_SETPOINT_TOPIC "setpoint/boil"
#define BOIL_ACTUAL_TOPIC "sensor/boil"
#define MASH_ACTUAL_TOPIC "sensor/mash"

// The value of the Rref resistor. Use 430.0 for PT100 and 4300.0 for PT1000
static const double RREF_KETTLE     = 430.0;
static const double RREF_MASH       = 430.0;
// The 'nominal' 0-degrees-C resistance of the sensor
// 100.0 for PT100, 1000.0 for PT1000
static const double RNOMINAL_KETTLE = 100.0;
static const double RNOMINAL_MASH   = 100.0;

#define MQTT_NAME     "brewhouse"
#define MQTT_PASSWORD "4SutKhR2ZEET2IU0PNhH"
#define MQTT_USER     "mqtt_user"
static const IPAddress MQTT_BROKER(192, 168, 1, 198);

static const byte mac[] = { 0xA6, 0x61, 0x0A, 0xAE, 0x89, 0xDE }; //physical mac address
static const IPAddress ip(192,168,1,177);

static const int PeriodPWM = 2000;

static const double ThreshPWR = 5;   // Float stored as int, last digit is decimal place
static const double Hysteresis = 1;  //

EthernetClient _net;

#endif
