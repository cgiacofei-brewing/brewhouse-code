BASE_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
LIBRARY_DIR        := $(BASE_DIR)/lib

ESPTOOL            := $(BASE_DIR)/hardware/esp8266/tools/esptool/esptool
ESPOTA             := $(BASE_DIR)/hardware/esp8266/tools/espota.py
TARGET_DIR         := $(BASE_DIR)/build-esp8266
BIN_DIR            := $(BASE_DIR)/bin

all: $(BIN_DIR)/$(SKETCH).bin

upload: $(TARGET_DIR)/$(SKETCH).bin
	$(ESPTOOL) -v -cd nodemcu -cb 115200 -cp $(MONITOR_PORT) -ca 0x00000 -cf $(TARGET_DIR)/$(SKETCH).bin

ota: $(TARGET_DIR)/$(SKETCH).bin
	$(ESPOTA) -d -r -i $(ESP_IP) -I $(OTA_SERVER) -p 8266 -P 8266 -a $(OTA_PASSWD) -f $(TARGET_DIR)/$(SKETCH).bin

clean:
	rm -rf $(TARGET_DIR)

monitor:
	screen $(MONITOR_PORT) 115200

library:
	$(eval NEW_LIB = $(LIBRARY_DIR)/$(LIB))
	mkdir -p $(NEW_LIB)
	$(eval UC = $(shell echo '$(LIB)' | tr '[:lower:]' '[:upper:]'))

	@ echo "#ifndef $(UC)_h" >  $(NEW_LIB)/$(LIB).h
	@ echo "#define $(UC)_h" >> $(NEW_LIB)/$(LIB).h
	@ echo ""                >> $(NEW_LIB)/$(LIB).h
	@ echo "#endif"          >> $(NEW_LIB)/$(LIB).h

.PHONY: test library upload ota clean monitor

$(BIN_DIR)/$(SKETCH).bin:
	@ mkdir -p $(TARGET_DIR)

	arduino-cli compile -logger=machine \
	--fqbn=$(BOARD_FQBN) \
	--library "$(subst $(space),$(sep),$(strip $(sort $(dir $(wildcard $(LIBRARY_DIR)/*/)))))" \
	--build-path "$(TARGET_DIR)" \
	--warnings=none \
	--verbose \
	"$(SKETCH_DIR)/$(SKETCH)"

	@ mv $(TARGET_DIR)/$(SKETCH).bin $(BIN_DIR)/
